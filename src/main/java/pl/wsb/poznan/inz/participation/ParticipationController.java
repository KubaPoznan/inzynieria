package pl.wsb.poznan.inz.participation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ParticipationController {
    private final ParticipationRepository participationRepository;
    private final ParticipationValidation participationValidation;

    @Autowired
    public ParticipationController(ParticipationRepository participationRepository, ParticipationValidation participationValidation) {
        this.participationRepository = participationRepository;
        this.participationValidation = participationValidation;
    }
    @DeleteMapping(value = "/participation")
    public ResponseEntity<Void> removeParticipation(@RequestParam long id) {
        participationRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/participation")
    public ResponseEntity<Void> addParticipation(@RequestBody Participation participation) {
        if (!participationValidation.isValid(participation.getLecture(),participation.getUser())) {
                return ResponseEntity.badRequest().build();
            }
        participationRepository.save(participation);
        return ResponseEntity.ok().build();
    }
}

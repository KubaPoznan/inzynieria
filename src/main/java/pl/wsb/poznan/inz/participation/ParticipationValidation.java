package pl.wsb.poznan.inz.participation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.wsb.poznan.inz.lectures.Lecture;
import pl.wsb.poznan.inz.users.ApplicationUser;

import java.time.LocalDate;
import java.util.List;

@Component
public class ParticipationValidation {
  /*isEmptySpace
  isBeforeStartDate
  isParallelClass*/
  @Autowired ParticipationRepository participationRepository;
  private Lecture lecture;
  private ApplicationUser applicationUser;

  private void init(Lecture lecture,ApplicationUser applicationUser){
    this.lecture = lecture;
    this.applicationUser = applicationUser;
  }
  public boolean isValid(Lecture lecture,ApplicationUser applicationUser){
    init(lecture,applicationUser);
    return isEmptySpace()&& isBeforeStartDate() && !isParallelClass();
  }

  private boolean isEmptySpace() {
    return participationRepository.countByLecture(lecture) < (lecture.getCapacity());
  }

  private boolean isBeforeStartDate() {
    return LocalDate.now().isBefore(lecture.getLectureStart());
  }

  private boolean isParallelClass() {
    List<Participation> participationList =
        participationRepository.findByApplicationUser(applicationUser);
    return startIsBetween(participationList) || endIsBetween(participationList);
  }

  private boolean startIsBetween(List<Participation> participationList) {
    return participationList.stream()
        .map(participation -> participation.getLecture().getLectureStart())
        .anyMatch(
            localDate ->
                localDate.isBefore(lecture.getLectureEnd())
                    && localDate.isAfter(lecture.getLectureStart()));
  }

  private boolean endIsBetween( List<Participation> participationList) {
    return participationList.stream()
        .map(participation -> participation.getLecture().getLectureEnd())
        .anyMatch(
            localDate ->
                localDate.isBefore(lecture.getLectureEnd())
                    && localDate.isAfter(lecture.getLectureStart()));
  }
}

package pl.wsb.poznan.inz.participation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.wsb.poznan.inz.lectures.Lecture;
import pl.wsb.poznan.inz.users.ApplicationUser;

import java.util.List;
@Repository
public interface ParticipationRepository extends JpaRepository<Participation, Long> {
  List<Participation> findByApplicationUser(ApplicationUser applicationUser);
  Long countByLecture (Lecture lecture);

}

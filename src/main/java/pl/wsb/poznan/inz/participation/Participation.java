package pl.wsb.poznan.inz.participation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.wsb.poznan.inz.lectures.Lecture;
import pl.wsb.poznan.inz.users.ApplicationUser;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Participation {
    @Id
    @GeneratedValue
    private Long id;
    @OneToOne
    private Lecture lecture;
    @OneToOne
    private ApplicationUser user;
}

package pl.wsb.poznan.inz.users;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
class UserController {

        private final UserRepository userRepository;
        private final ApplicationUserService applicationUserServiceImpl;

    @Autowired
        public UserController(UserRepository userRepository, ApplicationUserService applicationUserServiceImpl) {
        this.userRepository = userRepository;
        this.applicationUserServiceImpl = applicationUserServiceImpl;
    }
        @DeleteMapping(value = "/user")
         public ResponseEntity<Void> removeUser() {
            userRepository.deleteById(applicationUserServiceImpl.getCurrentlyLoggedUserReference().getId());
            return ResponseEntity.noContent().build();
        }

        @PostMapping(value = "/user")
        public ResponseEntity<Void> addUser(@RequestParam String email, @RequestParam String password) {
            if (EmailValidator.getInstance().isValid(email)) {
              if (applicationUserServiceImpl.checkIfUserExistByEmail(email).isPresent()) {
                return ResponseEntity.badRequest().build();
              }
              UserDTO userDTO = new UserDTO();
              userDTO.setEmail(email);
              userDTO.setPassword(password);
              applicationUserServiceImpl.save(userDTO);
              return ResponseEntity.ok().build();
            }
            return ResponseEntity.badRequest().build();
        }
    }

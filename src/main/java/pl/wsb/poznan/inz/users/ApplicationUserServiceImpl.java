package pl.wsb.poznan.inz.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
class ApplicationUserServiceImpl implements ApplicationUserService, UserDetailsService{
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserRepository userRepository;
    @Autowired
    public ApplicationUserServiceImpl(BCryptPasswordEncoder bCryptPasswordEncoder, UserRepository userRepository) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userRepository = userRepository;
    }
    @Override
    public void save(UserDTO userDTO) {
        ApplicationUser user = new ApplicationUser();
        user.setEmail(userDTO.getEmail());
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        userRepository.save(user);
    }
    @Override
    public UserDetails loadUserByUsername(String username) {
        ApplicationUser applicationUser = userRepository.findByEmail(username);
        if(applicationUser==null) {
            throw new UsernameNotFoundException(username);
        }
        return applicationUser;
    }
    @Override
    public Long getCurrentlyLoggedId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication == null) {
            return null;
        }
        if(authentication.getPrincipal() instanceof ApplicationUser) {
            ApplicationUser applicationUser = (ApplicationUser) authentication.getPrincipal();
            return applicationUser.getId();
        }
        return null;
    }
    @Override
    public ApplicationUser getCurrentlyLoggedUserReference() {
        Long currentlyLoggedId = getCurrentlyLoggedId();
        if(currentlyLoggedId == null) {
            return null;
        }
        return userRepository.getOne(currentlyLoggedId);
    }

    public Optional<ApplicationUser> checkIfUserExistByEmail(String email){
        ApplicationUser applicationUser = userRepository.findByEmail(email);
        return Optional.ofNullable(applicationUser);
    }
}

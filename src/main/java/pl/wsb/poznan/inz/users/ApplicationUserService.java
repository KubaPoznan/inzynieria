package pl.wsb.poznan.inz.users;


import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

public interface ApplicationUserService {
    void save(UserDTO userDTO);
    Long getCurrentlyLoggedId();
    ApplicationUser getCurrentlyLoggedUserReference();
    UserDetails loadUserByUsername(String username);
    Optional<ApplicationUser> checkIfUserExistByEmail(String email);
}

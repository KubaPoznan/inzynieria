package pl.wsb.poznan.inz.lectures;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.wsb.poznan.inz.users.ApplicationUser;
import pl.wsb.poznan.inz.users.ApplicationUserService;

import java.util.List;

@RestController
class LectureController {
    private final LectureRepository lectureRepository;
    private final ApplicationUserService applicationUserServiceImpl;

    @Autowired
    public LectureController(LectureRepository lectureRepository, ApplicationUserService applicationUserServiceImpl) {
        this.lectureRepository = lectureRepository;
        this.applicationUserServiceImpl = applicationUserServiceImpl;
    }


    @GetMapping(value = "/lecture")
    public ResponseEntity<List<Lecture>> getLecture() {
        ApplicationUser user = applicationUserServiceImpl.getCurrentlyLoggedUserReference();
        return ResponseEntity.ok(lectureRepository.findByApplicationUser(user));
    }
    @GetMapping(value = "/lecture/")
    public ResponseEntity<List<Lecture>> getLectureByFieldOfStudy(@RequestParam String fieldOfStudy){
        return ResponseEntity.ok(lectureRepository.findByFieldOfStudy(fieldOfStudy));
    }

    @DeleteMapping(value = "/lecture/{id}")
    public ResponseEntity<Void> removeLecture(@PathVariable Long id) {
        lectureRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/lecture")
    public ResponseEntity<Void> addLecture(@RequestBody List<Lecture> lectureList) {
        lectureRepository.saveAll(lectureList);
        return ResponseEntity.ok().build();
    }
}

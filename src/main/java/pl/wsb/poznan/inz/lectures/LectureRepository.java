package pl.wsb.poznan.inz.lectures;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.wsb.poznan.inz.users.ApplicationUser;

import java.util.List;
@Repository
public interface LectureRepository extends CrudRepository<Lecture,Long> {
    List<Lecture> findByApplicationUser(ApplicationUser applicationUser);
    List<Lecture> findByFieldOfStudy(String fieldOfStudy);
}

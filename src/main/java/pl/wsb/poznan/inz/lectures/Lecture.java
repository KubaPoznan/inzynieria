package pl.wsb.poznan.inz.lectures;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import pl.wsb.poznan.inz.users.ApplicationUser;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@Builder
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Lecture {
    @Id
    @GeneratedValue
    private Long id;
    private LocalDate lectureStart;
    private LocalDate lectureEnd;
    private Integer capacity;
    private String fieldOfStudy;
    @ManyToOne
    @JsonIgnore
    private ApplicationUser applicationUser;
    @ManyToOne
    @JsonIgnore
    private ApplicationUser lecturer;
}

package pl.wsb.poznan.inz.configuration.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class SecurityUtils {
    public static String generateAccessToken(String username, Long userId) {
        return generateToken(username, userId, SecurityConstants.ACCESS_TOKEN_EXPIRATION_TIME);
    }
    public static String generateRefreshToken(String username, Long userId) {
        return generateToken(username, userId, SecurityConstants.REFRESH_TOKEN_EXPIRATION_TIME);
    }
    private static String generateToken(String userName, long userId, long expiration) {
        return JWT.create()
                .withSubject(userName)
                .withClaim(SecurityConstants.USER_ID_CLAIM_NAME, userId)
                .withExpiresAt(new Date(System.currentTimeMillis() + expiration))
                .sign(Algorithm.HMAC512(SecurityConstants.SECRET.getBytes()));
    }
    public static void writeTokensToResponse(HttpServletResponse res, String authToken, String refreshToken) throws IOException {
        Cookie refreshTokenAuthCookie = new Cookie(SecurityConstants.REFRESH_TOKEN_AUTH_COOKIE_NAME, refreshToken);
        refreshTokenAuthCookie.setHttpOnly(true);
        res.addCookie(refreshTokenAuthCookie);
        res.addHeader("Content-Type", "application/json");
        res.getWriter().write(String.format("{ \"token\": \"%s\" }", authToken));
        res.getWriter().flush();
    }
}

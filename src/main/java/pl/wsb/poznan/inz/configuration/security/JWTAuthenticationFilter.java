package pl.wsb.poznan.inz.configuration.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import pl.wsb.poznan.inz.users.ApplicationUser;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final AuthenticationManager authenticationManager;
    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            ApplicationUser user = new ObjectMapper()
                    .readValue(request.getInputStream(), ApplicationUser.class);
            ArrayList<GrantedAuthority> roles = new ArrayList<>();
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                    user.getEmail(),
                    user.getPassword(),
                    roles
            );
            authenticationToken.setDetails(user);
            return authenticationManager.authenticate(authenticationToken);
        } catch (IOException e) {
            e.printStackTrace();
            throw new AttemptAuthenticationIOException(e.getMessage());
        }
    }
    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException {
        ApplicationUser applicationUser = (ApplicationUser) auth.getPrincipal();
        String token = SecurityUtils.generateAccessToken(applicationUser.getUsername(), applicationUser.getId());
        String refreshToken = SecurityUtils.generateRefreshToken(applicationUser.getUsername(), applicationUser.getId());
        SecurityUtils.writeTokensToResponse(res, token, refreshToken);
    }
}
package pl.wsb.poznan.inz.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.wsb.poznan.inz.lectures.LectureRepository;
import pl.wsb.poznan.inz.users.ApplicationUser;
import pl.wsb.poznan.inz.users.UserRepository;

@Configuration
public class Beans {

  private final UserRepository userRepository;
  private final LectureRepository lectureRepository;

  @Autowired
  public Beans(UserRepository userRepository, LectureRepository lectureRepository) {
    this.userRepository = userRepository;
    this.lectureRepository = lectureRepository;
  }

  @Bean
  public BCryptPasswordEncoder bCryptPasswordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  CommandLineRunner commandLineRunner() {
    return args -> {
      String email = "admin@admin.pl";
      ApplicationUser applicationUser =
          ApplicationUser.builder()
              .email(email)
              .password("$2y$12$A4j38Ggzr0Xxa6XJYDElE.s/PhawszJvzV0vVrHPf3ZD6n6pm9HEi")
              .build();


    };
  }
}

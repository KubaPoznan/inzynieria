package pl.wsb.poznan.inz.configuration.security;


import lombok.experimental.UtilityClass;

@UtilityClass
final class SecurityConstants {
    public static final String SECRET = "XXXXXXXXXXXXXXXX";
    //TODO 5 days for now as frontend doesnt use refresh but it should!
    public static final long ACCESS_TOKEN_EXPIRATION_TIME = 432000000;
    public static final long REFRESH_TOKEN_EXPIRATION_TIME = 2_592_000_000L; //30 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String REFRESH_TOKEN_AUTH_COOKIE_NAME = "auth";
    public static final String SIGN_UP_URL = "/users";
    public static final String REFRESH_TOKEN_URL = "/users/refresh";
    public static final String USER_ID_CLAIM_NAME = "userId";
}

package pl.wsb.poznan.inz.configuration.security;


class AttemptAuthenticationIOException extends RuntimeException {
    public AttemptAuthenticationIOException(String message) {
        super(message);
    }
}
